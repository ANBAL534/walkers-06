﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

/// <summary>
/// //////////////////////////////////////////////////////////////////////////////////////
/// ///////////  ARTIFICIAL INTELLIGENCE  MINION Script 
/// Author : 	Miguel Angel Fernandez Graciani
/// Date :	2021-02-07
/// Observations :
///     - THIS IS AN ARTIFICIAL INTELLIGENT SCRIPT
///     - You must call to the "public void moveOn(Vector3 directionForce, float movUnits)" in "ScMinionControl" to move the minion
///     - You must Change it to define the artiicial intelligence of this minion
/// </summary>
public class ScMinionAI_Far : MonoBehaviour {

    public float attackDistance = 15f;
    public GameObject selectedProfit = null;

    protected DateTime date_lastChamge;  // 
    protected double periodMilisec;

    protected Vector3 movement;  // Direction of the force that will be exerted on the gameobject
    protected float minionsMovUnits;  //  Amount of force that will be exerted on the gameobject


    // Use this for initialization
    /// <summary>
    /// ///////////  ARTIFICIAL INTELLIGENCE  MINION Script 
    /// Author : 	
    /// Date :	
    /// Observations :
    /// </summary>
    void Start()
    {

        date_lastChamge = DateTime.Now; // We initialize the date value
        periodMilisec = 1500f;  // We change each "periodoMiliseg"/1000 seconds

        movement = new Vector3(0.0f, 0.0f, 0.0f); // We initialize the date value
        minionsMovUnits = 1f; // We initialize the date value
    }  // FIn de - void Start()

    // Update is called once per frame
    /// <summary>
    /// ///////////  ARTIFICIAL INTELLIGENCE  MINION Script 
    /// Author : 	
    /// Date :	
    /// Observations :
    /// </summary>
    void Update()
    {

    }  // FIn de - void Update()

    /// <summary>
    /// //////////////////////////////////////////////////////////////////////////////////////
    /// ///////////  FixedUpdate()
    /// Author : 	Miguel Angel Fernandez Graciani
    /// Date :	2021-02-07
    /// Observations :
    ///     - THIS IS AN ARTIFICIAL INTELLIGENT SCRIPT
    ///     - You must Change it to define the artiicial intelligence of this player
    ///     - This one is only an example to manage the player
    /// </summary>
    void FixedUpdate()
    {
        // Basic attack for minion
        // minionsMovUnits = ScGameGlobalData.maxMinionsMovUnits;
        //
        // // Select the enemy commander
        // GameObject enemyCommander = null;
        // foreach (GameObject player in GameObject.FindGameObjectsWithTag("Player"))
        // {
        //     if (player.GetComponent<ScPlayerControl>().Team != this.GetComponent<ScMinionControl>().Team)
        //     {
        //         enemyCommander = player;
        //         break;
        //     }
        // }
        //
        // movement = enemyCommander.transform.position - transform.position;
        // movement = new Vector3(movement.x, 0, movement.z);
        //
        // // CALLING TO THIS FUNCTION YOU CAN MANAGE THE ELEMENT WITH THE ARTIFICIAL INTELLIGENCE THAT YOU MUST DEVELOP
        // GetComponent<ScMinionControl>().moveOn(movement, minionsMovUnits);

        minionsMovUnits = ScGameGlobalData.maxMinionsMovUnits;
        
        // Select Random profit to protect
        if (selectedProfit == null)
        {
            var profits = GameObject.FindGameObjectsWithTag("Profit");
            var profit = profits[Random.Range(0, profits.Length)];
            selectedProfit = profit;
        }
        
        // Find and measure distance with enemy commander
        GameObject enemyCommander = null;
        foreach (GameObject player in GameObject.FindGameObjectsWithTag("Player"))
        {
            if (player.GetComponent<ScPlayerControl>().Team != this.GetComponent<ScMinionControl>().Team)
            {
                enemyCommander = player;
                break;
            }
        }
        
        if (Mathf.Abs(Vector3.Distance(enemyCommander.transform.position, transform.position)) < attackDistance)
        {
            // If near, attack the enemy commander
            movement = enemyCommander.transform.position - transform.position;
            movement = new Vector3(movement.x, 0, movement.z);
        }
        else
        {
            // Move to selected profit if the enemy commander is far
            movement = selectedProfit.transform.position - transform.position;
            movement = new Vector3(movement.x, 0, movement.z);
        }
        
        
        // If our commander is very near make him space
        
        // Find and measure distance with enemy commander
        GameObject ourCommander = null;
        foreach (GameObject player in GameObject.FindGameObjectsWithTag("Player"))
        {
            if (player.GetComponent<ScPlayerControl>().Team == this.GetComponent<ScMinionControl>().Team)
            {
                ourCommander = player;
                break;
            }
        }
        
        if (Mathf.Abs(Vector3.Distance(ourCommander.transform.position, transform.position)) < 3f)
        {
            var mov = ourCommander.transform.position - transform.position;
            mov = new Vector3(mov.x, 0, mov.z) * -1 * 100;
            movement = mov;
        }
        
        // CALLING TO THIS FUNCTION YOU CAN MANAGE THE ELEMENT WITH THE ARTIFICIAL INTELLIGENCE THAT YOU MUST DEVELOP
        GetComponent<ScMinionControl>().moveOn(movement, minionsMovUnits);
    }  // Fin de - void FixedUpdate()

}  // Fin de - public class ScMinionAI_Far : MonoBehaviour {
