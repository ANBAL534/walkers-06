﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;


/// <summary>
/// //////////////////////////////////////////////////////////////////////////////////////
/// ///////////  ARTIFICIAL INTELLIGENCE  player Script 
/// Author : 	Miguel Angel Fernandez Graciani
/// Date :	2021-02-07
/// Observations :
///     - THIS IS AN ARTIFICIAL INTELLIGENT SCRIPT
///     - You must call to the "public void moveOn(Vector3 directionForce, float movUnits)" in "ScPlayerControl" to move the player 
///     - You must Change it to define the artiicial intelligence of this player
/// </summary>
public class ScPlayerAI_Far : MonoBehaviour {

    public float minionFearDistance = 15.0f;
    private bool isWallHit = false;
    public Vector3 escapePoint = Vector3.zero;
    private Rigidbody rb;
    
    DateTime date_lastChamge;
    protected double periodMilisec;

    protected Vector3 movement;  // Direction of the force that will be exerted on the gameobject
    protected float playersMovUnits;  //  Amount of force that will be exerted on the gameobject

    /// <summary>
    /// ///////////  ARTIFICIAL INTELLIGENCE  MINION Script 
    /// Author : 	
    /// Date :	
    /// Observations :
    /// </summary>
    void Start()
    {
        date_lastChamge = DateTime.Now; // We initialize the date value
        periodMilisec = 1000f;  // We change each "periodoMiliseg"/1000 seconds

        movement = new Vector3(0.0f, 0.0f, 0.0f); // We initialize the date value
        playersMovUnits = 1f; // We initialize the date value

        rb = GetComponent<Rigidbody>();
    }  // FIn de - void Start()

    // Update is called once per frame
    /// <summary>
    /// ///////////  ARTIFICIAL INTELLIGENCE  MINION Script 
    /// Author : 	
    /// Date :	
    /// Observations :
    /// </summary>
    void Update()
    {

    }  // FIn de - void Update()

    /// <summary>
    /// //////////////////////////////////////////////////////////////////////////////////////
    /// ///////////  FixedUpdate()
    /// Author : 	Miguel Angel Fernandez Graciani
    /// Date :	2021-02-07
    /// Observations :
    ///     - THIS IS AN ARTIFICIAL INTELLIGENT SCRIPT
    ///     - You must Change it to define the artiicial intelligence of this player
    ///     - This one is only an example to manage the player
    /// </summary>
    void FixedUpdate()
    {
        // Every "timeWhitoutChange_ms" milliseconds we modify the value of "movement" and "minionsMovUnits"
        DateTime dateNow = DateTime.Now;
        TimeSpan timeWhitoutChange = dateNow - date_lastChamge;

        double timeWhitoutChange_ms = timeWhitoutChange.TotalMilliseconds;

        if (ScGameGlobalData.Team_Far_Control == "manual")
        {
            float moveHorizontal = Input.GetAxis("Horizontal");
            float moveVertical = Input.GetAxis("Vertical");

            //        float moveHorizontal = 0;
            //        float moveVertical = 0;
            //        if (Input.GetKeyDown("i")) { moveVertical = 1f; }
            //        if (Input.GetKeyDown("k")) { moveVertical = -1f; }
            //        if (Input.GetKeyDown("j")) { moveHorizontal = -1f; }
            //        if (Input.GetKeyDown("l")) { moveHorizontal = 1f; }

            // We calculate the direction and quantity of movement
            movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
            playersMovUnits = 25f;
        }  // Fin de - if (ScGameGlobalData.Team_Near_Control == "manual")
        else if (ScGameGlobalData.Team_Far_Control == "randon")
        {
            if (timeWhitoutChange_ms > periodMilisec)
            {
                // We calculate the direction and quantity of movement
                // We obtain "movement" and "minionsMovUnits" randonly
                float move_X = Random.Range(-1.0f, 1f);
                float move_Z = Random.Range(-1f, 1f);
                playersMovUnits = Random.Range(0.0f, 1f);

                playersMovUnits = playersMovUnits * ScGameGlobalData.maxPlayersMovUnits;
                movement = new Vector3(move_X, 0.0f, move_Z);

                date_lastChamge = dateNow;  // We actualizate date_lastChamge
            }
        } // Fin de - else if (ScGameGlobalData.Team_Near_Control == "randon")
        else if (ScGameGlobalData.Team_Far_Control == "ai")
        {
            AIFunction();
        } // Fin de - else if (ScGameGlobalData.Team_Near_Control == "randon")
        else { /*Debug.Log("From ScPlayerAI_Far => FixedUpdate => Error 001");*/ }

        // CALlING TO THIS FUNCTION YOU CAN MANAGE THE ELEMENT WITH THE ARTIFICIAL INTELLIGENCE THAT YOU MUST DEVELOP
        GetComponent<ScPlayerControl>().moveOn(movement, playersMovUnits);
    }  // Fin de - void FixedUpdate()


        public void AIFunction()
    {
        playersMovUnits = ScGameGlobalData.maxPlayersMovUnits;

        // Select and order by distance all the profits
        var profits = GameObject.FindGameObjectsWithTag("Profit");
        profits = profits.OrderBy(x => Vector3.Distance(this.transform.position,x.transform.position)).ToArray();

        // Select the enemy commander
        GameObject enemyCommander = null;
        foreach (GameObject player in GameObject.FindGameObjectsWithTag("Player"))
        {
            if (player.GetComponent<ScPlayerControl>().Team != this.GetComponent<ScPlayerControl>().Team)
            {
                enemyCommander = player;
                break;
            }
        }

        // Find the closest profit that is far from the enemy commander
        if (profits.Length > 0)
        {
            var objectiveProfit = profits[0];
            for (int i = 0; i < profits.Length; i++)
            {
                if (Mathf.Abs(Vector3.Distance(enemyCommander.transform.position, objectiveProfit.transform.position)) <
                    Mathf.Abs(Vector3.Distance(this.transform.position, objectiveProfit.transform.position)))
                    objectiveProfit = profits[i];
            }
        
            // Move toward the selected profit
            var vect = objectiveProfit.transform.position - this.transform.position - rb.velocity / 2;
            movement = new Vector3(vect.x, 0.0f, vect.z) * 10;
        }
        
        // Search for the enemy minions and order them by distance
        var enemyMinions = new List<GameObject>();
        foreach (GameObject minion in GameObject.FindGameObjectsWithTag("Minion"))
        {
            if (minion.GetComponent<ScMinionControl>().Team != GetComponent<ScPlayerControl>().Team)
                enemyMinions.Add(minion);
        }
        enemyMinions = enemyMinions.OrderBy(x => Vector3.Distance(this.transform.position,x.transform.position)).ToList();
        
        // Run away from the enemy minion if very near, take into account the commander and enemy minion speed for range
        if (enemyMinions.Count > 0 && 
            Mathf.Abs(Vector3.Distance(enemyMinions[0].transform.position, transform.position)) <
            (minionFearDistance * (rb.velocity.magnitude / 20)) + (minionFearDistance * (enemyMinions[0].GetComponent<Rigidbody>().velocity.magnitude / 20)) + 3)
        {
            var mov = enemyMinions[0].transform.position - transform.position;
            mov = new Vector3(mov.x, 0, mov.z) * -1 * 100;

            // If we are running away but a wall was hit, search a escape point
            if (isWallHit && escapePoint.Equals(Vector3.zero))
            {
                escapePoint = GetEscapePoint(transform.position);
            }
            
            // If we have a escape point and we are cornered run towards it
            if (isWallHit && !escapePoint.Equals(Vector3.zero))
            {
                mov = escapePoint - transform.position;
            }
            
            movement = mov;
        }
        else
        {
            isWallHit = false;
            escapePoint = Vector3.zero;
        }
        
        Debug.DrawLine(transform.position, movement + transform.position, Color.red);
    }

    private Vector3 GetEscapePoint(Vector3 transformPosition)
    {
        var hitpoints = new List<Vector3>();

        for (int i = -1; i <= 1; i++)
        {
            for (int j = -1; j <= 1; j++)
            {
                if (i == j && i == 0)
                    continue;
                RaycastHit hit;
                if (Physics.Raycast(transformPosition, new Vector3(i , 0, j), out hit))
                {
                    hitpoints.Add(hit.point);
                }
            }
        }
        
        return hitpoints.OrderBy(x => Vector3.Distance(transformPosition,x)).Last();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.name.Contains("wall"))
        {
            isWallHit = true;
        }
    }


}  // Fin de - public class ScPlayerAI_Far : MonoBehaviour {
